# aarvinproperties.com

WSGI application written in Python for [aarvinproperties.com](). Features a CMS and database for managing properties on the wesbite.

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
What things you need to install the software and how to install them:

#### 1. python2.7

The latest Python 2.7 (2.7.14) is recommended for maximum security and compatibility:
```bash
$ sudo apt update
$ sudo apt install python2.7 python-pip
```

#### 2. Install virtualenv (recommended)

Using a virtual environment keeps dependencies (and their versions) separate from global packages.
```bash
$ sudo -H pip install virtualenv
```

#### 3. Node.js, NPM & SASS (optional)
To edit and compile the sass into css, the repo includes a _`Gruntfile.js`_ to do it automatically.

#### Node.js (LTS) and NPM:
```bash
$ cd ~
$ curl -sL https://deb.nodesource.com/setup_8.x -o nodesource_setup.sh
$ sudo bash nodesource_setup.sh
$ sudo apt install nodejs
```
Verify:
```bash
$ node -v
v8.10.0
$ npm -v
5.6.0
```

#### SASS
There are other (faster) implementations of SASS that could be installed at this point but for simplicity the Node.js version should suffice.
```bash
$ npm install -g sass
```
Verify:
```bash
$ sass --version
1.2.0 compiled with dart2js 2.0.0-dev.48.0
```

#### Grunt & plugins

To install Grunt.js and its plugins:
```bash
$ cd app/
$ npm install
```

To run grunt and automatically compile SASS to CSS:
```bash
$ grunt
```

This looks at the _`Gruntfile.js`_ which configures the files to be compiled, then auto-prefixed and lastly, auto-reloads the page.

### Installing

First, `cd` into root directory

#### virtualenv (skip if not using)
```bash
$ virtualenv venv
$ source activate
```
If using virtualenv, assume commands from here on are ran within the virtual environment.

#### Python dependencies
The following command installs the required packages listed in _`requirements.txt`_.

`sudo -H` needed if not in a virtualenv.

```bash
$ [sudo -H ]pip install -r requirements.txt
```

#### Debug Mode (for development)
```bash
$ echo "FLASK_ENV=development" > .envrc
```
The environment variable be automatically set when Flask is run.

## Deployment

### Local/development
If running locally, `$ python run.py` or `$ flask run`

Output:
```bash
 * Environment: development
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: XXX-XXX-XXX
```

### Production
For a production server, configure your server to run _`run.py`_ or the _`app.app`_ Python object as a WSGI application to redirect requests to:

#### uWSGI example
The following would go into your _`.ini`_ file

- APP_ROOT - root directory of the application
- PORT - port uWSGI app will run on

_`uwsgi.ini`_
```INI
[uwsgi]
home = APP_ROOT
wsgi-file = %(home)/run.py
socket = 127.0.0.1:PORT
callable = app
module = app
pythonpath = %(home)
daemonize = %(home)/uwsgi.log
pidfile = %(home)/uwsgi.pid

master = true
processes = 2
...
```

#### Nginx (with uWSGI) example
To redirect the requests to an nginx server to your application, put the following in your _`nginx.conf`_.

```nginx
...
server {
    server_name DOMAIN;
    location / {
        include uwsgi_params;
        uwsgi_pass 127.0.0.1:PORT;
    }

    ...
}
...
```

## Built With

[Flask](https://flask.pocoo.org) - Python microframework for web and extensions:

- [Flask-Bcrypt](https://flask-bcrypt.readthedocs.io/en/latest/) - bcrypt hashing
- [Flask-Images](https://mikeboers.github.io/Flask-Images/) - dynamic image resizing
- [Flask-Login](https://flask-login.readthedocs.io/en/latest/) - user session management
- [Flask-Principal](https://pythonhosted.org/Flask-Principal/) - authentication & user info provider
- [Flask-SQLAlchemy](http://flask-sqlalchemy.pocoo.org/2.3/) - adds [SQLAlchemy](http://www.sqlalchemy.org/) support to Flask
- [Flask-Uploads](https://pythonhosted.org/Flask-Uploads/) - handle file uploading
- [Flask-WTF](https://flask-wtf.readthedocs.io/en/stable/) - integration of Flask and [WTForms](https://wtforms.readthedocs.io/en/stable/)

[google-maps-services-python](https://github.com/googlemaps/google-maps-services-python) - [Google Maps Api Web Services](https://developers.google.com/maps/web-services/overview) in Python

[Grunt.js](https://gruntjs.com/) - Task runner for automation

[SASS](https://sass-lang.com/) - CSS preprocessor

## Authors
- **Jay Tauron** - [jdxt](https://gitlab.com/jdxt) on GitLab

## License
See the [LICENSE.md](README.md) file for details

## Acknowledgements
TODO: add hat tips