// service worker for app
// if ('serviceWorker' in navigator) {
//   window.addEventListener('load', function() {
//     navigator.serviceWorker.register('/sw.js').then(function(registration) {
//         registration.unregister();
//       // Registration was successful
//       console.log('ServiceWorker registration successful with scope: ', registration.scope);
//     }, function(err) {
//       // registration failed :(
//       console.log('ServiceWorker registration failed: ', err);
//     });
//   });
// }

if ('serviceWorker' in navigator) {
    navigator.serviceWorker.getRegistrations().then(function(registrations) {
        for (let registration of registrations) {
            registration.unregister()
        }
    })
}

// page javascript
const $doc = $(document);
$doc.ready(function() {
    new WOW().init();

    const $window = $(window);
    $window.scrollTop(0);

    var clipboard = new ClipboardJS('.contact-item');

    clipboard.on('success', function(e) {
        toastr.options = {
            "positionClass": "toast-bottom-center"
        };
        toastr['info'](e.text + ' copied to clipboard');

        e.clearSelection();
    });

    clipboard.on('error', function(e) {
        console.error('Action:', e.action);
        console.error('Trigger:', e.trigger);
    });

    $nav = $('#navbar');

    let $searchBox = $('#search-box');
    let $results = $('#results');

    function mainSearch(obj) {
        $results.empty();

        args = {
            q: obj.q || null,
            r: obj.r || 2,
            lat: obj.lat || null,
            lng: obj.lng || null,
            rob: $('#search-drop').val() == 'rent' ? 1 : 0
        };

        $.get('/search', args, function(data) {

            $nav.addClass('expand');
            $searchBox.removeClass('loading');

            if (!data.length) {
                toastr['info']('Sorry, no results')
            } else {
                $results.html(data).addClass('show');
            }

        });
    }

    $searchBox.submit(function(e) {
        let search = $(this).find('#postcode-input').val();

        $results.removeClass('show');

        if (search) {
            $(this).addClass('loading');
        } else {
            return false;
        }

        mainSearch({q: search});

        return false;
    });

    if (!navigator.geolocation) {
        $('#loc-icon').hide();
    }

    var loc_cache;

    $('#loc-icon').click(function() {
        $results.removeClass('show');
        $searchBox.addClass('loading');

        if (loc_cache) {
            mainSearch(loc_cache)
        } else {
            var positionOptions = {};
            positionOptions.timeout = 5000;

            navigator.geolocation.getCurrentPosition(function(pos) {

                args = {
                    lat: pos.coords.latitude,
                    lng: pos.coords.longitude
                };

                loc_cache = args;

                mainSearch(args);
            }, function(e) {
                $searchBox.removeClass('loading');
                toastr['warning']('Sorry, your location is not available at this time, please try a search term.');
            }, positionOptions);
        }
    });

    let $searchLabel = $('label[for=postcode-input]');
    $('#postcode-input').keyup(function(e) {
        if ($(this).val().length) $searchLabel.addClass('focused');
        else $searchLabel.removeClass('focused');
    });

    $('body').on('click', '#results .result-container', function() {
        let $this = $(this);

        if (!$this.data('img-loaded')) {
            $this.data('img-loaded', true);

            $this.find('.carousel img[data-src]').each(function(i, img) {
                let $that = $(this);
                $that.attr('src', $that.attr('data-src'));
                $that[0].onload = function() {
                    $(this).removeAttr('data-src');
                };
            });
        }

        // let $scrollTo = $this.find('.carousel');
        // $this.animate({
        //     scrollTop: $scrollTo.offset().top - $this.offset().top + $this.scrollTop()
        // })
    });

});
