$(document).ready(function() {
    let $body = $('body');

    $body.on('click', '.delete-button', function(e) {
        let $this = $(this);
        let id = $this.data('id');
        $.ajax({
            url: '/api/property',
            type: 'DELETE',
            data: {id: id},
            success: function(data) {
                $('#' + id).hide();
                $('#delete-' + id).hide();

                $.get('/flashes', function(data) {
                    $body.append(data);
                });
            }
        });
    });
});