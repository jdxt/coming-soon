$(document).ready(function() {
    var bar = document.getElementById('js-progressbar');

    UIkit.upload('.js-upload', {

        url: '/api/pics',
        multiple: true,

        beforeSend: function () {
            // console.log('beforeSend', arguments);
            arguments[0].data.append('_id', $('#_id-input').val());
        },
        // beforeAll: function () {
        //     console.log('beforeAll', arguments);
        // },
        // load: function () {
        //     console.log('load', arguments);
        // },
        // error: function () {
        //     console.log('error', arguments);
        // },
        complete: function () {
            // console.log('complete', arguments);
            $('#image-grid').append(arguments[0].response);
        },

        loadStart: function (e) {
            // console.log('loadStart', arguments);

            bar.removeAttribute('hidden');
            bar.max = e.total;
            bar.value = e.loaded;
        },

        progress: function (e) {
            // console.log('progress', arguments);

            bar.max = e.total;
            bar.value = e.loaded;
        },

        loadEnd: function (e) {
            // console.log('loadEnd', arguments);

            bar.max = e.total;
            bar.value = e.loaded;
        },

        completeAll: function () {
            // console.log('completeAll', arguments);

            setTimeout(function () {
                bar.setAttribute('hidden', 'hidden');
                $('#_id-input').val($('.img-card').data('id'));
            }, 500);
        }

    });
});