$(document).ready(function() {

    let $alerts = $('.form-alert');
    let $inputs = $('input');

    $inputs.on('keyup mouseup', function() {
        $(this).removeClass('uk-form-danger')
            .closest('label').siblings('.form-alert')
            .addClass('uk-hidden')
            .html();
    });

    $('#submit').click(function(e) {
        e.preventDefault();

        let data = $('form').serialize();

        $.ajax({
            url: '/signup',
            type: 'post',
            data: data,
            success: function(data, status, xhr) {
                window.location.replace('/admin');
            },
            error: function(xhr, status, error) {
                if (xhr.status == 400) {
                    // BAD REQUEST
                    var obj = xhr.responseJSON;
                    console.log(obj);
                    $alerts.addClass('uk-hidden');
                    $inputs.removeClass('uk-form-danger');

                    for (let key in obj) {
                        $('#' + key)
                            .addClass('uk-form-danger')
                            .closest('.uk-margin')
                            .find('.form-alert')
                            .html(obj[key])
                            .removeClass('uk-hidden');
                    }
                    toastr['warning']('Input is required.');
                }
            }
        });
    });

});