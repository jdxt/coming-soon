$(document).ready(function() {
    let $form = $('#upload-form')

    let $icons = $('a[uk-icon="icon: pencil"]').not('.keep');
    $icons.click(function() {
        let $this = $(this);
        $this.hide();

        let targets = 'input, label, select, textarea';
        let $target = $this.siblings(targets);

        if ($target.is('label')) {
            $target = $target.find('input');
        }
        $target.prop('disabled', false);

        if ($target.is('[type=file]')) {
            $.getScript('/js/pics.js');
            $this.parent().removeClass('disabled');
        }
    });

    let $imgInput = $('#img-list');
    let $imgGrid = $('#image-grid');
    $imgGrid.mouseup(function() {
        if ($imgInput.prop('disabled')) {
            $imgInput.prop('disabled', false)
        }

        let images = []
        $(this).find('.img-card').each(function(i, el) {
            images.push($(this).data('pic'));
            $(this).find('span.uk-label').html(i + 1);
        });
        $imgInput.val(images.join(';'));
    });

    $('#submit').click(function(e) {
        let data = $form.serializeArray();
        console.log(data);
        $.ajax({
            url: '/api/property',
            method: 'put',
            data: data,
            statusCode: {
                409: function() {
                    toastr['error']('Sorry, property does not exist. It may have been deleted.');
                }
            },
            complete: function() {
                window.location.replace('/admin/view');
            }
        });
        e.preventDefault();
    });

    $('body').on('click', '.delete-button', function() {
        let $this = $(this);
        let pic = $this.data('pic');
        let id = $this.data('id');

        $.ajax({
            url: '/api/pics',
            type: 'delete',
            data: {
                pic: pic,
                id: id
            },
            statusCode: {
                204: function() {
                    $('#' + pic.split('.')[0]).hide();
                    toastr['success']('Image deleted');
                },
                409: function(xhr, status, error) {
                    toastr['error']('Sorry, image does not exist. It may have been deleted.');
                }
            },
        });
    });
});