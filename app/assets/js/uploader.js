$(document).ready(function() {

    let $alerts = $('.form-alert');
    let $inputs = $('input');

    $('#submit').click(function(e) {
        e.preventDefault();

        let id = $('#image-grid').find('.img-card').data('id');
        $('#_id-input').val(id);

        let images = []
        $('#image-grid').find('.img-card').each(function() {
            images.push($(this).data('pic'));
        });
        if (images.length) {
            $('#img-list').val(images.join(';'));
        }

        let data = $('#upload-form').serialize();

        $.ajax({
            url: '/api/property',
            type: 'post',
            data: data,
            success: function(data, status, xhr) {
                window.location.replace('/admin/view');
            },
            error: function(xhr, status, error) {
                if (xhr.status == 400) {
                    // BAD REQUEST
                    var obj = xhr.responseJSON;
                    $alerts.addClass('uk-hidden');
                    $inputs.removeClass('uk-form-danger');

                    for (let key in obj) {
                        $('#' + key)
                            .addClass('uk-form-danger')
                            .closest('.uk-margin')
                            .find('.form-alert')
                            .html(obj[key])
                            .removeClass('uk-hidden');
                    }
                    toastr['warning']('Input is required.');
                }
            }
        });
    });

});