$(document).ready(function() {
    $(window).on('load', function() {
        window.domLoaded = true;
        $('.container.init-loading').removeClass('init-loading');
    });

    if (!window.domLoaded) {
        $(window).trigger('load');
    }
});