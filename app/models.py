from uuid import uuid4

from sqlalchemy import Column, Integer, String, Float, Boolean, BLOB, DateTime, Unicode

from . import bcrypt, db


class User(db.Model):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, autoincrement=True)
    username = Column(String(64), unique=True, nullable=False)
    _password = Column(String(128), nullable=False)
    perms = Column(Integer, nullable=False)

    def __init__(self, username, password, role):
        self.username = username
        self._password = bcrypt.generate_password_hash(password)
        self.perms = role

    @property
    def password(self):
        return self._password

    def is_correct_password(self, plaintext):
        if bcrypt.check_password_hash(self._password, plaintext):
            return True
        return False

    # compatibility for Flask-Principal
    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def __repr__(self):
        return '<User %r>' % self.username


class Property(db.Model):
    __tablename__ = 'properties'

    id = Column(String(32), primary_key=True, nullable=False)
    text_id = Column(String(5), autoincrement=True)
    estate_name = Column(String(64))
    estate_type_id = Column(Integer)

    price_per_month = Column(Float)

    floor_space = Column(Float)
    balcony_space = Column(Float)
    num_balconies = Column(Integer)
    num_bedrooms = Column(Integer)
    num_bathrooms = Column(Integer)
    num_garages = Column(Integer)
    num_parking_spaces = Column(Integer)
    pets_allowed = Column(Boolean)

    summary = Column(String(512))
    description = Column(String(1024))

    address = Column(String(256))
    postcode = Column(String(11))
    lon = Column(Float)
    lat = Column(Float)
    place_id = Column(String(64))

    images = Column(BLOB)

    uploaded = Column(DateTime)

    buy_or_rent = Column(Integer)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
