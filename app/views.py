import os
import shutil
import random
from urlparse import urlparse, urljoin
from datetime import timedelta, datetime
import json
from uuid import uuid4
import marshal
from math import sin, cos, acos, pi
from pprint import pprint as pp

from flask import send_from_directory, render_template, request,\
        jsonify, url_for, redirect, abort, flash, session
from flask.views import MethodView
from flask_login import login_user, logout_user, login_required, login_manager
from flask_principal import RoleNeed, Identity, AnonymousIdentity,\
    identity_changed, identity_loaded
from flask_uploads import UploadSet, configure_uploads, IMAGES
from flask_images import resized_img_src

from jinja2 import Environment

from sqlalchemy import text
import googlemaps

from . import app, db, ESTATE_TYPES, user_permission, admin_permission
from .forms import UserPassForm, CreateUserForm, PropertyForm
from .models import User, Property

app.login_manager.login_view = 'admin'

photos = UploadSet('photos', IMAGES)
configure_uploads(app, (photos,))

gmaps = googlemaps.Client(key='AIzaSyBOoPkmqPfRXdd3irq0z6TjYoeqphJ3-HU')


def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and ref_url.netloc == test_url.netloc


if app.debug:
    @app.route('/assets/<path:path>')
    def send(path):
        return send_from_directory('assets', path)


@app.context_processor
def inject_debug():
    return dict(debug=app.debug, method=request.method)


@app.template_filter('printer')
def printer(_text):
    print _text
    return _text


@app.before_request
def make_session_permanent():
    session.permanent = True
    app.permanent_session_lifetime = timedelta(minutes=20)


@app.route('/')
def index():
    return render_template('home.html.j2')


# needed for static files in /
@app.route('/<string:_file>')
def root_files(_file):
    return send_from_directory('static', filename=_file)


@app.route('/tmp/<path:path>')
@login_required
def tmp_dir(path):
    return send_from_directory('tmp', path)


@app.route('/search', methods=['GET', 'POST'])
def search():
    # args: q r lat lng rob
    term = request.args['q']
    if term:
        # search term within london
        kwargs = {
            'location': (51.507364, -0.127638),
            'radius': 30000,
        }
        res = gmaps.places(term, **kwargs)
        coords = tuple(res['results'][0]['geometry']['location'].values())
        term = '"' + term + '"'
    else:
        coords = tuple(map(float, [request.args['lat'], request.args['lng']]))
        term = "your location"

    _list = []
    results = db.session.query(Property).filter(Property.buy_or_rent == request.args['rob'])

    for result in results:
        lat = result.lat
        lon = result.lon

        _lat = coords[0]
        _lon = coords[1]

        dist = ((acos(sin(_lat * pi / 180) * sin(lat * pi / 180) + \
            cos(_lat * pi / 180) * cos(lat * pi / 180) * cos((_lon - lon) * \
            pi / 180)) * 180 / pi) * 60 * 1.1515)

        if dist <= float(request.args['r']):
            if result.images:
                result.images = marshal.loads(result.images)
            result.estate_type = ESTATE_TYPES[result.estate_type_id][1]
            _list.append(result)

    return render_template('result.html.j2', data=_list, term=term)


@app.route('/flashes')
def flashes():
    return render_template('just_flashes.html.j2')


def gen_id(_id=None):
    if _id is None or not db.session.query(Property).filter(Property.id == _id):
        return gen_id(uuid4().hex)
    return _id


photo_dir = 'app/assets/img/'
@app.route('/api/pics', methods=['POST', 'DELETE'])
@login_required
def pic_upload():

    if request.method == 'POST':
        _file = request.files.getlist('files[]')[0]
        fname = photos.save(_file, name=gen_id()+'.')

        _id = request.form.get('_id') or gen_id()

        photo_dir_id = photo_dir + _id
        tmp = True
        # only move to a dir if exists
        if os.path.exists(photo_dir_id):
            path = photo_dir_id + '/' + fname
            os.rename(photos.path(fname), path)

            prop = Property.query.get(_id)

            if prop.images:
                imgs = marshal.loads(prop.images)
            else:
                imgs = []
            imgs.append(fname)
            prop.images = marshal.dumps(imgs)
            db.session.commit()
            tmp = False
        return render_template('img_preview.html.j2', pic=fname, _id=_id, tmp=tmp)

    if request.method == 'DELETE':
        data = request.form.to_dict()
        _id = data['id']
        pic = data['pic']

        path = photo_dir + _id + '/' + pic
        if os.path.exists(path):
            os.remove(path)

        prop = Property.query.get(_id)
        if not prop:
            return 'Object not found.', 409

        imgs = marshal.loads(prop.images)
        imgs.remove(pic)
        prop.images = marshal.dumps(imgs)
        db.session.commit()

        return '', 204

    return 'This endpoint accepts POST and DELETE requests.', 405


@app.route('/api/property', methods=['GET', 'POST', 'PUT', 'DELETE'])
@login_required
def api():
    if request.method == 'GET':
        props = Property.query.all()
        _list = []
        for prop in props:
            _list.append(prop.as_dict())

        code = 200 if len(_list) else 404
        return jsonify(json_list=_list), 200

    if request.method == 'POST':
        form = PropertyForm()

        if not form.validate():
            return jsonify(form.errors), 400

        _id = request.form['_id'] or gen_id()
        int_id = 'AVN' + str(db.session.query(Property).count() + 1).zfill(3)

        address = ''
        for x in range(1, 5):
            address += form['addr' + str(x)].data + ';'

        res = gmaps.places(address)['results']
        if res:
            pl = res[0]
        else:
            pl = {
                'place_id': None,
                'geometry': {'location': {
                    'lng': None,
                    'lat': None
                }}
            }

        pics = request.form['img-list']

        if pics:
            photo_dir_id = photo_dir + _id
            if not os.path.exists(photo_dir_id):
                os.makedirs(photo_dir_id)

            pics = pics.split(';')
            for pic in pics:
                shutil.move('app/tmp/' + pic, photo_dir_id)
            pics = marshal.dumps(pics)

        props = {
            'id': _id,
            'text_id': int_id,
            'estate_name': form.estate_name.data,
            'estate_type_id': form.estate_type_id.data,
            'price_per_month': form.price_per_month.data,
            'floor_space': form.floor_space.data,
            'balcony_space': form.balcony_space.data,
            'num_balconies': form.num_balconies.data,
            'num_bedrooms': form.num_bedrooms.data,
            'num_bathrooms': form.num_bathrooms.data,
            'num_garages': form.num_garages.data,
            'num_parking_spaces': form.num_parking_spaces.data,
            'pets_allowed': form.pets_allowed.data,
            'summary': form.summary.data,
            'description': form.description.data,
            'address': address,
            'postcode': form.postcode.data.upper(),
            'lon': pl['geometry']['location']['lng'],
            'lat': pl['geometry']['location']['lat'],
            'place_id': pl['place_id'],
            'images': pics,
            'uploaded': datetime.now(),
            'buy_or_rent': form.buy_or_rent.data
        }

        prop = Property(**props)
        db.session.add(prop)
        db.session.commit()

        flash('Property uploaded', 'success')

        return jsonify(prop.as_dict()), 201

    if request.method == 'PUT':
        data = request.form.to_dict()

        data['pets_allowed'] = True if data['pets_allowed'] else False

        _id = data['_id']
        query = db.session.query(Property).filter_by(id=_id)
        prop = query.first()

        if not prop:
            return 'Object to update not found', 409

        if prop.address:
            old_add = prop.address.split(';')
        else:
            old_add = []

        for key in data.keys():
            if key == 'img-list':
                data['images'] = marshal.dumps(data['img-list'].split(';'))
                del data['img-list']
            elif key.startswith('addr'):
                idx = int(key[-1]) - 1
                old_add[idx] = data[key]
                del data[key]
            elif key == 'estate_type':
                data['estate_type_id'] = data[key]
                del data[key]

        if old_add:
            data['address'] = ';'.join(old_add)

        del data['csrf_token']
        del data['_id']

        if len(data):
            query.update(data)
            db.session.commit()

        flash('Property \'%s\' updated successfully.' % prop.estate_name, 'success')
        return jsonify(data), 200

    if request.method == 'DELETE':
        _id = request.form.get('id')
        prop = Property.query.get(_id)

        if prop == None:
            flash('Property already deleted', 'warning')
            return _id + ' not found in database', 200

        name = prop.estate_name

        db.session.delete(prop)
        db.session.commit()

        img_dir = os.path.join(os.getcwd(), 'app/assets/img/' + _id)
        if os.path.isdir(img_dir):
            shutil.rmtree(img_dir)

        flash('Property \'%s\' deleted' % name, 'success')
        return '', 204

    return 'This endpoint accepts GET, POST, PUT and DELETE requests', 405


@app.route('/admin', methods=['GET', 'POST'])
def admin():
    form = UserPassForm()

    # POST
    if request.method == 'POST' and form.validate():
        user = User.query.filter_by(username=form.username.data).first()

        if user and user.is_correct_password(form.password.data):
            login_user(user)
            flash('%s logged in successfully.' % user.username, 'success')

            identity_changed.send(app, identity=Identity(user.id))

            nxt = request.form['next']
            if not is_safe_url(nxt):
                return abort(400)

            return redirect(nxt or url_for('view'))

        flash('Incorrect login details.', 'error')
        return redirect(url_for('admin'))

    return render_template('admin.html.j2', form=form)


@app.route('/logout')
@login_required
def logout():
    logout_user()

    for key in ('identity.name', 'identity.auth_type'):
        session.pop(key, None)

    identity_changed.send(app, identity=AnonymousIdentity())

    flash('Logged out successfully', 'success')
    return redirect(url_for('admin'))


@app.route('/signup', methods=['GET', 'POST'])
@admin_permission.require(http_exception=403)
def signup():
    form = CreateUserForm()

    if request.method == 'POST':

        if not form.validate():
            return jsonify(form.errors), 400

        user = User.query.filter_by(username=form.username.data).first()

        if user is not None:
            return jsonify({
                'username': 'Username already exists.'
            }), 400

        if form.role.data == 'admin':
            role = 0
        elif form.role.data == 'user':
            role = 1

        user = User(username=form.username.data, password=form.password.data, role=role)

        db.session.add(user)
        db.session.commit()
        flash('User %s signed up successfully' % user.username, 'success')
        return redirect(url_for('admin'))

    return render_template('signup.html.j2', form=form)


@identity_loaded.connect_via(app)
def on_identity_loaded(sender, identity):
    try:
        user = User.query.get(identity.id)
        role = 'user'

        if user.perms == 0:
            role = 'admin'
        elif user.perms >= 1:
            role = 'user'

        identity.provides.add(RoleNeed(role))
    except Exception as e:
        print e

@app.route('/admin/view')
@login_required
def view():
    result = Property.query.all()
    for ppt in result:
        if ppt.images:
            print marshal.loads(ppt.images)
            ppt.images = marshal.loads(ppt.images)

    return render_template('view_properties.html.j2', result=result)


@app.route('/admin/upload')
@app.route('/admin/edit/<string:_id>')
@login_required
def upload(_id=None, form=None):
    # defaults, for /admin/upload
    prop = None
    method = 'post'
    images = None

    # handle /admin/edit/:_id
    if _id:
        prop = Property.query.get_or_404(_id)

        # parse address
        addr_list = prop.address.split(';')
        for i, line in enumerate(addr_list):
            setattr(prop, 'addr' + str(i + 1), line)
        del prop.address

        method = 'put'
        images = None
        if prop.images:
            images = marshal.loads(prop.images)

    if not form:
        form = PropertyForm(obj=prop)

    return render_template('upload.html.j2', form=form, method=method, images=images, _id=_id)


@app.errorhandler(404)
def handle_404(e):
    return jsonify({
        'url': request.base_url,
        'message': 'sorry url doesn\'t exist'
    }), 404


@app.errorhandler(403)
def handle_403(e):
    return jsonify({
        'url': request.base_url,
        'message': 'you need to login or you don\'t have permission'
    }), 403


if __name__ == "__main__":
    app.run()
