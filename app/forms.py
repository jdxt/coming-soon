from jinja2 import Environment, PackageLoader, select_autoescape
from flask_wtf import FlaskForm
from wtforms import Form, TextField, PasswordField, validators, RadioField,\
    SelectField, IntegerField, BooleanField, FileField, FloatField,\
    TextAreaField, SubmitField

from . import ESTATE_TYPES

env = Environment(
    loader=PackageLoader('app', 'templates'),
    autoescape=select_autoescape(['html', 'xml'])
)


def field(_type, *args, **kwargs):
    def caller(self, **kwargs):
        if 'method' in kwargs.keys():
            edit = kwargs['method'] == 'put'
        else:
            edit = False

        options = ''
        if _type == 'Select':
            for val, opt, selected in self.iter_choices():
                options += self.widget.render_option(val, opt, selected)
        elif _type == 'Radio':
            for choice in self.iter_choices():
                print choice
                print dir(self)
                options += '\
                <label>\
                    <input class="uk-radio" type="radio" name="%(name)s" value="%(val)s">\
                    %(label)s\
                </label>' % {'name': self.short_name, 'val': choice[0], 'label': choice[1]}

        data = {
            'label': self.label.text,
            'edit': edit,
            'id': self.id,
            'val': '' if self.object_data is None else self.object_data,
            'disabled': 'disabled' if edit else '',
            'type': _type,
            'options': options
        }

        return env.get_template('form_macro.html.j2').render(**data)

    obj = eval(_type + 'Field')
    class Field(obj):
        __call__ = caller

    return Field(*args, **kwargs)


required = validators.InputRequired
opt = validators.optional
lngth = validators.Length
eqlTo = validators.EqualTo


class UserPassForm(FlaskForm):
    username = TextField('username', validators=[required()])
    password = PasswordField('password', validators=[required()])


class CreateUserForm(FlaskForm):
    username = field('Text', 'Username:', validators=[required()])
    password = field('Password', 'Password:', validators=[required()])

    password_conf = field('Password', 'Confirm password:', validators=\
        [required(), eqlTo('password', message='Passwords must match')])

    role = field('Radio', 'Role:',\
        choices=[('admin', 'admin'), ('user', 'user')], validators=[required()])


class PropertyForm(FlaskForm):
    estate_name = field('Text', 'Name (internal):', validators=[required()])

    estate_type_id = field('Select', 'Type:', choices=ESTATE_TYPES, coerce=int)

    buy_or_rent = field('Select', 'Listing type:', choices=[(0, 'buy'), (1, 'rent')], coerce=int)
    price_per_month = field('Float', 'Price (pm for rent):', validators=[required()])

    floor_space = field('Float', 'Floor space (sq. ft.):', validators=[opt()])
    balcony_space = field('Float', 'Balcony space (sq. ft.):', validators=[opt()])
    num_balconies = field('Integer', 'No of balconies:', validators=[opt()])
    num_bathrooms = field('Integer', 'No of bathrooms:', validators=[opt()])
    num_bedrooms = field('Integer', 'No of bedrooms:', validators=[opt()])
    num_garages = field('Integer', 'No of garages:', validators=[opt()])
    num_parking_spaces = field('Integer', 'No of parking spaces:', validators=[opt()])
    pets_allowed = field('Boolean', 'Pets allowed?:')

    summary = field('TextArea', 'Summary:', validators=[lngth(max=512)])
    description = field('TextArea', 'Description:', validators=[lngth(max=1024)])

    addr1 = field('Text', 'Address line 1:', validators=[lngth(max=32), required()])
    addr2 = field('Text', 'Address line 2:', validators=[lngth(max=32)])
    addr3 = field('Text', 'Address line 3:', validators=[lngth(max=32)])
    addr4 = field('Text', 'Address line 4:', validators=[lngth(max=32)])

    postcode = field('Text', 'Post code', validators=[lngth(max=11), required()])
    images = field('File', 'pics', render_kw={'multiple': True})
    submit = SubmitField('submit')
