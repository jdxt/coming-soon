import os
from datetime import timedelta

from flask import Flask
from flask_bcrypt import Bcrypt
from flask_sqlalchemy import SQLAlchemy
from flask_principal import Principal, Permission, RoleNeed
from flask_login import LoginManager
from flask_images import Images

app = Flask(__name__, static_folder=None)
app.jinja_env.auto_reload = True
app.config.update(
    BCRYPT_LOG_ROUNDS=12,

    SQLALCHEMY_TRACK_MODIFICATIONS=False,
    SQLALCHEMY_DATABASE_URI='sqlite:///db/database.db',

    SECRET_KEY='\x11\xebl\x1a\x8f9\x85\x00\x04\x85W%\xeae\x05\x99\\F\xd7i\xb3c\xf7\xb9',
    REMEMBER_COOKIE_DURATION=timedelta(minutes=20),

    UPLOADED_PHOTOS_DEST=os.path.abspath('app/tmp'),
    TEMPLATES_AUTO_RELOAD=True
)

bcrypt = Bcrypt(app)
db = SQLAlchemy(app, session_options={'autoflush': False})
images = Images(app)

principals = Principal(app)
user_permission = Permission(RoleNeed('user'))
admin_permission = user_permission.union(Permission(RoleNeed('admin')))

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'
login_manager.session_protection = 'strong'
login_manager.login_message = 'You need to log in to access this page.'
login_manager.login_message_category = 'info'

ESTATE_TYPES = [
    (0, 'Flat'),
    (1, 'Detached'),
    (2, 'Semi-detached'),
    (3, 'Terraced'),
    (4, 'End of terrace'),
    (5, 'Cottage'),
    (6, 'Bungalow')
]


def lookup_url(endpoint, values):
    if endpoint == 'static':
        if app.debug:
            return '/assets/' + values['filename']
        return 'https://static.aarvinproperties.com/' + values['filename']
    return None


def external_url_handler(error, endpoint, values):
    url = lookup_url(endpoint, values)
    if url is None:
        exc_type, exc_valu, tb = sys.exec_info()
        if exc_value is error:
            raise exc_type, exc_value, tb
        else:
            raise error
    return url


app.url_build_error_handlers.append(external_url_handler)

from .models import User

@login_manager.user_loader
def load_user(user_id):
    return User.query.filter(User.id == user_id).first()

from . import views
