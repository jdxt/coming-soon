module.exports = function(grunt) {
	grunt.initConfig({

		sass: {
			dist: {
				options: {
					style: 'compressed',
				},
				files: {
					['sass/compiled/main.css']: 'sass/main.sass',
					['sass/compiled/init.css']: 'sass/init.sass',
					['sass/compiled/admin.css']: 'sass/admin.sass'
				},
			},
		},

		autoprefixer: {
			options: {
				map: true,
			},
			dist: {
				files: {
					'templates/init.css': 'sass/compiled/init.css',
					'assets/css/styles.css': 'sass/compiled/main.css',
					'assets/css/admin.css': 'sass/compiled/admin.css'
				},
			},
		},

		watch: {
			css: {
				files: ['sass/*.sass'],
				tasks: ['sass', 'autoprefixer'],
			},

			templates: {
				files: ['templates/*.html.j2', '*.py'],
				tasks: [],
				options: {
					livereload: true,
				},
			},
		},

	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-autoprefixer');

	grunt.registerTask('default', ['watch']);
};